"use strict";

var _faker = _interopRequireDefault(require("faker"));

var _framework = require("../framework");

var _CountryBuilder = require("../framework/builder/CountryBuilder.js");

var _PromoCodeBuilder = require("../framework/builder/PromoCodeBuilder.js");

var _ProfileBuilder = require("../framework/builder/ProfileBuilder.js");

var _TicketBuilder = require("../framework/builder/TicketBuilder.js");

var _RefferalsBuilder = require("../framework/builder/RefferalsBuilder.js");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

describe('testsuit for clientCountries Controller', function () {
  test('Authorization for admin', function _callee() {
    var data, r;
    return regeneratorRuntime.async(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            reporter.description("Authorization for admin");
            reporter.startStep("Авторизация админа");
            reporter.endStep();
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context.next = 6;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 6:
            r = _context.sent;
            expect(r.status).toBe(200);

          case 8:
          case "end":
            return _context.stop();
        }
      }
    });
  });
  test('Get all countries', function _callee2() {
    var data, rr, token, r;
    return regeneratorRuntime.async(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context2.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context2.sent;
            token = rr.body.result.authToken;
            console.log('token ', token);
            _context2.next = 8;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().countries().getAllCountries(token));

          case 8:
            r = _context2.sent;
            expect(r.status).toBe(200);

          case 10:
          case "end":
            return _context2.stop();
        }
      }
    });
  });
  test('Search cities by default country or by all countries', function _callee3() {
    var data, rr, token, demo, params, r;
    return regeneratorRuntime.async(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context3.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context3.sent;
            token = rr.body.result.authToken;
            console.log('token ', token);
            demo = new _CountryBuilder.CountryBuilder();
            params = demo // поля котрые отправляем
            .SearchCities_important().SearchCities_limit().SearchCities_offset().SearchCities_useDefaultCountry().generate();
            console.log('params ', params);
            _context3.next = 11;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().countries().SearchCities(token, params));

          case 11:
            r = _context3.sent;
            expect(r.status).toBe(200);

          case 13:
          case "end":
            return _context3.stop();
        }
      }
    });
  });
  test('Search cities by country', function _callee4() {
    var data, rr, token, demo, params, r;
    return regeneratorRuntime.async(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context4.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context4.sent;
            token = rr.body.result.authToken;
            console.log('token ', token);
            demo = new _CountryBuilder.CountryBuilder();
            params = demo // поля котрые отправляем
            .SearchCities_important().SearchCities_limit().SearchCities_offset().SearchCities_query().generate();
            _context4.next = 10;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().countries().postCities(token, params));

          case 10:
            r = _context4.sent;
            expect(r.status).toBe(200);

          case 12:
          case "end":
            return _context4.stop();
        }
      }
    });
  });
});
describe('testsuit for adminPromoCode Controller', function () {
  test('Create Promocode type=GIFT', function _callee5() {
    var data, rr, token, demo, params, r;
    return regeneratorRuntime.async(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            data = {
              login: 'admin@technaxis.com',
              password: 'password'
            };
            _context5.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context5.sent;
            token = rr.body.result.authToken;
            console.log('token ', token);
            demo = new _PromoCodeBuilder.PromoCodeBuilder();
            params = demo // поля котрые отправляем
            .PromoCode_activationDate().PromoCode_code().PromoCode_discount_gift().PromoCode_discountTypeGift().PromoCode_disposable().PromoCode_expirationDate().PromoCode_name();
            console.log('params ', params);
            _context5.next = 11;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().promocode().CreatePromoCode(token, params));

          case 11:
            r = _context5.sent;
            console.log('create promocode', r.text);
            expect(r.body.result.discountType).toBe('GIFT');
            expect(r.status).toBe(201);

          case 15:
          case "end":
            return _context5.stop();
        }
      }
    });
  });
  test('Create Promocode type=PERCENT_FOR_PRODUCT', function _callee6() {
    var data, rr, token, demo, params, r;
    return regeneratorRuntime.async(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            data = {
              login: 'admin@technaxis.com',
              password: 'password'
            };
            _context6.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context6.sent;
            token = rr.body.result.authToken;
            console.log('token ', token);
            demo = new _PromoCodeBuilder.PromoCodeBuilder();
            params = demo // поля котрые отправляем
            .PromoCode_activationDate().PromoCode_code().PromoCode_discount().PromoCode_discountType().PromoCode_disposable().PromoCode_expirationDate().PromoCode_name();
            console.log('params ', params);
            _context6.next = 11;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().promocode().CreatePromoCode(token, params));

          case 11:
            r = _context6.sent;
            console.log('create promocode', r.text);
            expect(r.body.result.discountType).toBe('PERCENT_FOR_PRODUCT');
            expect(r.status).toBe(201);

          case 15:
          case "end":
            return _context6.stop();
        }
      }
    });
  });
  test('Get Promocode', function _callee7() {
    var data, rr, token, promoCodeId, r;
    return regeneratorRuntime.async(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            data = {
              login: 'admin@technaxis.com',
              password: 'password'
            };
            _context7.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context7.sent;
            token = rr.body.result.authToken;
            promoCodeId = Math.floor(Math.random() * Math.floor(10)) * 10 + 1;
            _context7.next = 8;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().promocode().GetPromoCode(token, promoCodeId));

          case 8:
            r = _context7.sent;
            expect(r.body.result.id).toBe(promoCodeId);
            expect(r.status).toBe(200);

          case 11:
          case "end":
            return _context7.stop();
        }
      }
    });
  });
  test('PUT Promocode', function _callee8() {
    var data, rr, token, promoCodeId, demo, params, r;
    return regeneratorRuntime.async(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            data = {
              login: 'admin@technaxis.com',
              password: 'password'
            };
            _context8.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context8.sent;
            token = rr.body.result.authToken; //console.log('token ', token);

            promoCodeId = Math.floor(Math.random() * Math.floor(10)) * 10 + 1; //80

            console.log('promoCodeId = ', promoCodeId);
            demo = new _PromoCodeBuilder.PromoCodeBuilder();
            params = demo // поля котрые отправляем
            .PromoCode_activationDate().PromoCode_code().PromoCode_discount().PromoCode_discountType().PromoCode_disposable().PromoCode_expirationDate().PromoCode_name().generate();
            _context8.next = 11;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().promocode().UpdatePromoCode(token, promoCodeId, params));

          case 11:
            r = _context8.sent;
            //console.log(r.text);
            expect(r.body.result.id).toBe(promoCodeId);
            expect(r.body.result.code).toBe(params.code);
            expect(r.status).toBe(200);

          case 15:
          case "end":
            return _context8.stop();
        }
      }
    });
  });
});
describe('testsuit for clientLevel Controller', function () {
  test('Get user levels', function _callee9() {
    var data, rr, token, r;
    return regeneratorRuntime.async(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context9.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context9.sent;
            token = rr.body.result.authToken; //console.log('token ', token);

            _context9.next = 7;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().clientLevel().getLevel(token));

          case 7:
            r = _context9.sent;
            //console.log(r.text);
            expect(r.status).toBe(200);

          case 9:
          case "end":
            return _context9.stop();
        }
      }
    });
  });
  test('Get user scales', function _callee10() {
    var data, rr, token, r;
    return regeneratorRuntime.async(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context10.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context10.sent;
            token = rr.body.result.authToken; //console.log('token ', token);

            _context10.next = 7;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().clientLevel().getUserScales(token));

          case 7:
            r = _context10.sent;
            //console.log(r.text);
            expect(r.status).toBe(200);

          case 9:
          case "end":
            return _context10.stop();
        }
      }
    });
  });
});
describe('testsuit for clientProfile Controller', function () {
  test('Get profile', function _callee11() {
    var data, rr, token, r;
    return regeneratorRuntime.async(function _callee11$(_context11) {
      while (1) {
        switch (_context11.prev = _context11.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context11.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context11.sent;
            token = rr.body.result.authToken; //console.log('token ', token);

            _context11.next = 7;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().clientprofile().getProfile(token));

          case 7:
            r = _context11.sent;
            //console.log(r.text);
            //expect(r.text.name).toEqual("Аленка");
            expect(r.body.result.id).toBe(99);
            expect(r.body.result.email).toEqual('sdgsdg@jlrt.rte');
            expect(r.body.result.phone).toEqual('+79274435697');
            expect(r.status).toBe(200);

          case 12:
          case "end":
            return _context11.stop();
        }
      }
    });
  });
  test('Update profile', function _callee12() {
    var data, rr, token, demo, params, r;
    return regeneratorRuntime.async(function _callee12$(_context12) {
      while (1) {
        switch (_context12.prev = _context12.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context12.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context12.sent;
            token = rr.body.result.authToken;
            demo = new _ProfileBuilder.ProfileBuilder();
            params = demo.Profile_name().Profile_referralCode().Profile_referralUrlVisible().Profile_newPassword().Profile_oldPassword().generate();
            _context12.next = 9;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().clientprofile().updateProfile(token, params));

          case 9:
            r = _context12.sent;
            console.log(r.text);
            expect(r.body.result.id).toBe(99);
            expect(r.body.result.name).toBe(params.name);
            expect(r.body.result.email).toEqual('sdgsdg@jlrt.rte');
            expect(r.body.result.phone).toEqual('+79274435697');
            expect(r.status).toBe(200);

          case 16:
          case "end":
            return _context12.stop();
        }
      }
    });
  });
  test('Get update profile avatar', function _callee13() {
    var data, rr, token, params, r;
    return regeneratorRuntime.async(function _callee13$(_context13) {
      while (1) {
        switch (_context13.prev = _context13.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context13.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context13.sent;
            token = rr.body.result.authToken; //получаем токен и сохраняем
            //console.log('token ', token);

            params = {
              avatarId: 298
            };
            _context13.next = 8;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().clientprofile().updateProfileAvatar(token, params));

          case 8:
            r = _context13.sent;
            //console.log(r.text);
            expect(r.body.result.id).toBe(99);
            expect(r.body.result.email).toEqual('sdgsdg@jlrt.rte');
            expect(r.body.result.phone).toEqual('+79274435697');
            expect(r.body.result.avatar.id).toBe(params.avatarId);
            expect(r.status).toBe(200);

          case 14:
          case "end":
            return _context13.stop();
        }
      }
    });
  });
  test('Get profile summary', function _callee14() {
    var data, rr, token, r;
    return regeneratorRuntime.async(function _callee14$(_context14) {
      while (1) {
        switch (_context14.prev = _context14.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context14.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context14.sent;
            token = rr.body.result.authToken; //console.log('token ', token);

            _context14.next = 7;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().clientprofile().getProfileSummary(token));

          case 7:
            r = _context14.sent;
            //console.log(r.text);
            expect(r.body.result.id).toBe(99);
            expect(r.body.result.email).toEqual(data.login); //'sdgsdg@jlrt.rte'

            expect(r.body.result.phone).toEqual('+79274435697');
            expect(r.status).toBe(200);

          case 12:
          case "end":
            return _context14.stop();
        }
      }
    });
  });
}); // test('Get Payment info ', async () => {
//   const data = {
//     login: 'sdgsdg@jlrt.rte',
//     password: 'Password56',
//   };
//   const rr = await apiProvider().authorization().post(data);
//   const token = rr.body.result.authToken; //получаем токен и сохраняем
//   console.log('token ', token);
//   const orderId = ;
//   const r = await apiProvider().clientpayment().getPaymentInfo(token, orderId);
//   expect(r.status).toBe(200);
// });

test('Get level user ', function _callee15() {
  var data, rr, token, r;
  return regeneratorRuntime.async(function _callee15$(_context15) {
    while (1) {
      switch (_context15.prev = _context15.next) {
        case 0:
          data = {
            login: 'sdgsdg@jlrt.rte',
            password: 'Password56'
          };
          _context15.next = 3;
          return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

        case 3:
          rr = _context15.sent;
          token = rr.body.result.authToken; //console.log('token ', token);

          _context15.next = 7;
          return regeneratorRuntime.awrap((0, _framework.apiProvider)().clientevent().getEvent(token));

        case 7:
          r = _context15.sent;
          //console.log(r.text);
          expect(r.status).toBe(200);

        case 9:
        case "end":
          return _context15.stop();
      }
    }
  });
});
test('Update ref code ', function _callee16() {
  var data, rr, token, params, r;
  return regeneratorRuntime.async(function _callee16$(_context16) {
    while (1) {
      switch (_context16.prev = _context16.next) {
        case 0:
          data = {
            login: 'sdgsdg@jlrt.rte',
            password: 'Password56'
          };
          _context16.next = 3;
          return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

        case 3:
          rr = _context16.sent;
          token = rr.body.result.authToken; //console.log('token ', token);

          params = {
            code: _faker["default"].finance.bic()
          };
          _context16.next = 8;
          return regeneratorRuntime.awrap((0, _framework.apiProvider)().clientrefcode().UpdateRefCode(token, params));

        case 8:
          r = _context16.sent;
          //console.log(r.text);
          //console.log('params.code', params.code);
          expect(r.body.code).toEqual(params.code);
          expect(r.status).toBe(200);

        case 11:
        case "end":
          return _context16.stop();
      }
    }
  });
});
describe('testsuit for clientRefferals Controller', function () {
  test('Get refferals statistics ', function _callee17() {
    var data, rr, token, params, r;
    return regeneratorRuntime.async(function _callee17$(_context17) {
      while (1) {
        switch (_context17.prev = _context17.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context17.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context17.sent;
            token = rr.body.result.authToken; //console.log('token ', token);

            params = {
              lines: [1, 2]
            };
            _context17.next = 8;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().clintrefferals().GetRefferals(token, params));

          case 8:
            r = _context17.sent;
            //console.log(r.text);
            expect(r.status).toBe(200);

          case 10:
          case "end":
            return _context17.stop();
        }
      }
    });
  }); //ВЗЯЛА

  test('Search refferals ', function _callee18() {
    var data, rr, token, demo, params, r;
    return regeneratorRuntime.async(function _callee18$(_context18) {
      while (1) {
        switch (_context18.prev = _context18.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context18.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context18.sent;
            token = rr.body.result.authToken; //console.log('token ', token);

            demo = new _RefferalsBuilder.RefferalsBuilder();
            params = demo.getLimitOfRefferals().getLinesOfRefferals().getOffsetOfRefferals().generate();
            _context18.next = 9;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().clintrefferals().SearchRefferals(token, params));

          case 9:
            r = _context18.sent;
            //console.log(r.text);
            expect(r.body.result.data[0].line).toBe(1);
            expect(r.status).toBe(200);

          case 12:
          case "end":
            return _context18.stop();
        }
      }
    });
  });
});
describe('testsuit for client buy ticket Controller', function () {
  //ВЗЯЛА
  test('buy ticket ', function _callee19() {
    var data, rr, token, demo, params, r;
    return regeneratorRuntime.async(function _callee19$(_context19) {
      while (1) {
        switch (_context19.prev = _context19.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context19.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context19.sent;
            token = rr.body.result.authToken;
            demo = new _TicketBuilder.TicketBuilder();
            params = demo.getAmountTicketForBuy().getPartlyOnlinePaymentSumInKopecks().getPaymentMethod(false).getRedirectUrl().generate();
            _context19.next = 9;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().clentickets().buyTicket(token, params));

          case 9:
            r = _context19.sent;
            expect(r.body.result.status).toEqual('PAYMENT_REQUIRED');
            expect(r.status).toBe(201);

          case 12:
          case "end":
            return _context19.stop();
        }
      }
    });
  }); //ВЗЯЛА

  test('buy ticket by wallet', function _callee20() {
    var data, rr, token, demo, params, r;
    return regeneratorRuntime.async(function _callee20$(_context20) {
      while (1) {
        switch (_context20.prev = _context20.next) {
          case 0:
            data = {
              login: 'hft456@mjoi.tyr',
              password: 'Password56'
            };
            _context20.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context20.sent;
            token = rr.body.result.authToken;
            demo = new _TicketBuilder.TicketBuilder();
            params = demo.getAmountTicketForBuy().getPaymentMethod(true).generate();
            _context20.next = 9;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().clentickets().buyTicket(token, params));

          case 9:
            r = _context20.sent;
            expect(r.body.result.status).toEqual('SUCCESS');
            expect(r.status).toBe(201);

          case 12:
          case "end":
            return _context20.stop();
        }
      }
    });
  });
  test('count tickets ', function _callee21() {
    var data, rr, token, r;
    return regeneratorRuntime.async(function _callee21$(_context21) {
      while (1) {
        switch (_context21.prev = _context21.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context21.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context21.sent;
            token = rr.body.result.authToken; //console.log('token ', token);

            _context21.next = 7;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().clentickets().getTicketCount(token));

          case 7:
            r = _context21.sent;
            //console.log(r.text);
            expect(r.status).toBe(200);

          case 9:
          case "end":
            return _context21.stop();
        }
      }
    });
  });
  test('get tickets multiplier ', function _callee22() {
    var data, rr, token, r;
    return regeneratorRuntime.async(function _callee22$(_context22) {
      while (1) {
        switch (_context22.prev = _context22.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context22.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context22.sent;
            token = rr.body.result.authToken; //console.log('token ', token);

            _context22.next = 7;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().clentickets().getTicketsMultiplayer(token));

          case 7:
            r = _context22.sent;
            //console.log(r.text);
            expect(r.status).toBe(200);

          case 9:
          case "end":
            return _context22.stop();
        }
      }
    });
  }); //ВЗЯЛА

  test('search tickets ', function _callee23() {
    var data, rr, token, demo, params, r;
    return regeneratorRuntime.async(function _callee23$(_context23) {
      while (1) {
        switch (_context23.prev = _context23.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context23.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context23.sent;
            token = rr.body.result.authToken;
            demo = new _TicketBuilder.TicketBuilder();
            params = demo.getLimitOfTickets(true).getOffsetOfTickets().getSourcePayment().getWinningTickets().generate();
            _context23.next = 9;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().clentickets().SearchTickets(token, params));

          case 9:
            r = _context23.sent;
            expect(r.body.result.data[0].source).toEqual(params.source);
            expect(r.status).toBe(200);

          case 12:
          case "end":
            return _context23.stop();
        }
      }
    });
  });
  test('buy trial tickets by partly online', function _callee24() {
    var data, rr, token, params, r;
    return regeneratorRuntime.async(function _callee24$(_context24) {
      while (1) {
        switch (_context24.prev = _context24.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context24.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context24.sent;
            token = rr.body.result.authToken; //console.log('token ', token);

            params = {
              partlyOnlinePaymentSumInKopecks: 1000,
              paymentMethod: 'PARTLY_ONLINE',
              redirectUrl: 'https://matreshka.technaxis.com/cabinet/profile'
            };
            _context24.next = 8;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().clentickets().buyTrialTicket(token, params));

          case 8:
            r = _context24.sent;
            //console.log(r.text);
            expect(r.body.result.status).toEqual('PAYMENT_REQUIRED');
            expect(r.status).toBe(201);

          case 11:
          case "end":
            return _context24.stop();
        }
      }
    });
  });
  test('buy trial tickets by online', function _callee25() {
    var data, rr, token, params, r;
    return regeneratorRuntime.async(function _callee25$(_context25) {
      while (1) {
        switch (_context25.prev = _context25.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context25.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context25.sent;
            token = rr.body.result.authToken; //console.log('token ', token);

            params = {
              paymentMethod: 'ONLINE',
              redirectUrl: 'https://matreshka.technaxis.com/cabinet/profile'
            };
            _context25.next = 8;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().clentickets().buyTrialTicket(token, params));

          case 8:
            r = _context25.sent;
            //console.log(r.text);
            expect(r.body.result.status).toEqual('PAYMENT_REQUIRED');
            expect(r.status).toBe(201);

          case 11:
          case "end":
            return _context25.stop();
        }
      }
    });
  });
  test('get ticket by id', function _callee26() {
    var data, rr, token, ticketId, r;
    return regeneratorRuntime.async(function _callee26$(_context26) {
      while (1) {
        switch (_context26.prev = _context26.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context26.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context26.sent;
            token = rr.body.result.authToken; //console.log('token ', token);

            ticketId = 89860;
            _context26.next = 8;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().clentickets().getTicketById(token, ticketId));

          case 8:
            r = _context26.sent;
            //console.log(r.text);
            expect(r.body.result.id).toBe(ticketId);
            expect(r.body.result.number).toEqual('74478138');
            expect(r.body.result.source).toEqual('GIFT');
            expect(r.status).toBe(200);

          case 13:
          case "end":
            return _context26.stop();
        }
      }
    });
  }); //ВЗЯТЬ Истоия выигрышей билета

  test('Search ticket winning history', function _callee27() {
    var data, rr, token, ticketId, params, r;
    return regeneratorRuntime.async(function _callee27$(_context27) {
      while (1) {
        switch (_context27.prev = _context27.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context27.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context27.sent;
            token = rr.body.result.authToken; //console.log('token ', token);

            ticketId = 36534;
            params = {
              limit: 10,
              offset: 0,
              sorting: [{
                "order": "DESC",
                "type": "WIN_AMOUNT"
              }]
            };
            _context27.next = 9;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().clentickets().SearchTicketWinningHistory(token, ticketId, params));

          case 9:
            r = _context27.sent;
            //console.log(r.text);
            expect(r.status).toBe(200);

          case 11:
          case "end":
            return _context27.stop();
        }
      }
    });
  });
});
describe('testsuit for clientWallets Controller', function () {
  test('get my wallet', function _callee28() {
    var data, rr, token, r;
    return regeneratorRuntime.async(function _callee28$(_context28) {
      while (1) {
        switch (_context28.prev = _context28.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context28.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context28.sent;
            token = rr.body.result.authToken; //console.log('token ', token);

            _context28.next = 7;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().clientwallet().getMyWallet(token));

          case 7:
            r = _context28.sent;
            //console.log(r.text);
            expect(r.status).toBe(200);

          case 9:
          case "end":
            return _context28.stop();
        }
      }
    });
  });
});
describe('testsuit for clientWithdrawls Controller', function () {
  test('get user limits to withdrawal', function _callee29() {
    var data, rr, token, r;
    return regeneratorRuntime.async(function _callee29$(_context29) {
      while (1) {
        switch (_context29.prev = _context29.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context29.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context29.sent;
            token = rr.body.result.authToken;
            console.log('token ', token);
            _context29.next = 8;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().clientwithdrawls().getUserlimits(token));

          case 8:
            r = _context29.sent;
            //console.log(r.text);
            expect(r.status).toBe(200);

          case 10:
          case "end":
            return _context29.stop();
        }
      }
    });
  });
  test('get all withdrawal providers', function _callee30() {
    var data, rr, token, r;
    return regeneratorRuntime.async(function _callee30$(_context30) {
      while (1) {
        switch (_context30.prev = _context30.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context30.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context30.sent;
            token = rr.body.result.authToken; //получаем токен и сохраняем
            //console.log('token ', token);

            _context30.next = 7;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().clientwithdrawls().getAllWithdraws(token));

          case 7:
            r = _context30.sent;
            //console.log(r.text);
            expect(r.status).toBe(200);

          case 9:
          case "end":
            return _context30.stop();
        }
      }
    });
  });
});
describe('testsuit for Promo Controller', function () {
  test('Page promo', function _callee31() {
    var data, rr, token, params, types, index, r;
    return regeneratorRuntime.async(function _callee31$(_context31) {
      while (1) {
        switch (_context31.prev = _context31.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context31.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context31.sent;
            token = rr.body.result.authToken; //получаем токен и сохраняем
            //console.log('token ', token);

            params = {
              limit: 10,
              offset: 0
            };
            types = ['MATERIAL', 'ADVERTISING', 'TRAINING'];
            index = parseInt(Math.random(1, 2)); //console.log('i ', index);

            _context31.next = 10;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().clientpromomaterial().pagePromo(token, params, types[index]));

          case 10:
            r = _context31.sent;
            //console.log(r.text);
            expect(r.status).toBe(200);

          case 12:
          case "end":
            return _context31.stop();
        }
      }
    });
  });
});
describe('testsuit for winningHistories Controller', function () {
  test('page reviews', function _callee32() {
    var data, rr, token, params, r;
    return regeneratorRuntime.async(function _callee32$(_context32) {
      while (1) {
        switch (_context32.prev = _context32.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context32.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context32.sent;
            token = rr.body.result.authToken; //получаем токен и сохраняем
            //console.log('token ', token);

            params = {
              limit: 10,
              offset: 0
            };
            _context32.next = 8;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().clientwinninghistories().pageReviews(token, params));

          case 8:
            r = _context32.sent;
            //console.log(r.text);
            expect(r.status).toBe(200);

          case 10:
          case "end":
            return _context32.stop();
        }
      }
    });
  });
  test('Search winning histories', function _callee33() {
    var data, rr, token, params, r;
    return regeneratorRuntime.async(function _callee33$(_context33) {
      while (1) {
        switch (_context33.prev = _context33.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context33.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context33.sent;
            token = rr.body.result.authToken; //получаем токен и сохраняем
            //console.log('token ', token);

            params = {
              limit: 10,
              offset: 0,
              referralLines: [1, 2],
              referrerLines: [1],
              withReview: true
            };
            _context33.next = 8;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().clientwinninghistories().searchWinningHistories(token, params));

          case 8:
            r = _context33.sent;
            //console.log(r.text);
            expect(r.status).toBe(200);

          case 10:
          case "end":
            return _context33.stop();
        }
      }
    });
  });
  test('Get winning history', function _callee34() {
    var data, rr, token, historyId, r;
    return regeneratorRuntime.async(function _callee34$(_context34) {
      while (1) {
        switch (_context34.prev = _context34.next) {
          case 0:
            data = {
              login: 'sdgsdg@jlrt.rte',
              password: 'Password56'
            };
            _context34.next = 3;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().authorization().post(data));

          case 3:
            rr = _context34.sent;
            token = rr.body.result.authToken; //получаем токен и сохраняем

            historyId = 58225;
            _context34.next = 8;
            return regeneratorRuntime.awrap((0, _framework.apiProvider)().clientwinninghistories().getWinningHistory(token, historyId));

          case 8:
            r = _context34.sent;
            expect(r.body.result.id).toBe(historyId); //expect(r.body.result.winner.id).toBe(37);

            expect(r.status).toBe(200);

          case 11:
          case "end":
            return _context34.stop();
        }
      }
    });
  });
});