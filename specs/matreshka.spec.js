import faker from 'faker';
import { apiProvider } from '../framework';
import { CountryBuilder } from '../framework/builder/CountryBuilder.js';
import { PromoCodeBuilder } from '../framework/builder/PromoCodeBuilder.js';
import { ProfileBuilder } from '../framework/builder/ProfileBuilder.js';
import { TicketBuilder } from '../framework/builder/TicketBuilder.js';
import { RefferalsBuilder } from '../framework/builder/RefferalsBuilder.js';



describe('testsuit for clientCountries Controller', () => {

  test('Authorization for admin', async () => {
    reporter
      .description("Authorization for admin")

    reporter.startStep("Авторизация админа");
    reporter.endStep();


    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const r = await apiProvider().authorization().post(data);

    expect(r.status).toBe(200);
  });


  test('Get all countries', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken;

    console.log('token ', token);
    const r = await apiProvider().countries().getAllCountries(token);

    expect(r.status).toBe(200);
  });


  test('Search cities by default country or by all countries', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken;

    console.log('token ', token);

    const demo = new CountryBuilder();
    const params = demo // поля котрые отправляем
      .SearchCities_important()
      .SearchCities_limit()
      .SearchCities_offset()
      .SearchCities_useDefaultCountry()
      .generate();

    console.log('params ', params);

    const r = await apiProvider().countries().SearchCities(token, params);

    expect(r.status).toBe(200);
  });



  test('Search cities by country', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken;

    console.log('token ', token);

    const demo = new CountryBuilder();
    const params = demo // поля котрые отправляем
      .SearchCities_important()
      .SearchCities_limit()
      .SearchCities_offset()
      .SearchCities_query()
      .generate();


    const r = await apiProvider().countries().postCities(token, params);

    expect(r.status).toBe(200);
  });

});


describe('testsuit for adminPromoCode Controller', () => {
  test('Create Promocode type=GIFT', async () => {

    const data = {
      login: 'admin@technaxis.com',
      password: 'password',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken;

    console.log('token ', token);


    const demo = new PromoCodeBuilder();
    const params = demo // поля котрые отправляем
      .PromoCode_activationDate()
      .PromoCode_code()
      .PromoCode_discount_gift()
      .PromoCode_discountTypeGift()
      .PromoCode_disposable()
      .PromoCode_expirationDate()
      .PromoCode_name()

    console.log('params ', params);

    let r = await apiProvider().promocode().CreatePromoCode(token, params);
    console.log('create promocode', r.text);

    expect(r.body.result.discountType).toBe('GIFT');
    expect(r.status).toBe(201);
  });

  test('Create Promocode type=PERCENT_FOR_PRODUCT', async () => {

    const data = {
      login: 'admin@technaxis.com',
      password: 'password',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken;

    console.log('token ', token);


    const demo = new PromoCodeBuilder();
    const params = demo // поля котрые отправляем
      .PromoCode_activationDate()
      .PromoCode_code()
      .PromoCode_discount()
      .PromoCode_discountType()
      .PromoCode_disposable()
      .PromoCode_expirationDate()
      .PromoCode_name()

    console.log('params ', params);

    let r = await apiProvider().promocode().CreatePromoCode(token, params);
    console.log('create promocode', r.text);

    expect(r.body.result.discountType).toBe('PERCENT_FOR_PRODUCT');
    expect(r.status).toBe(201);
  });


  test('Get Promocode', async () => {

    const data = {
      login: 'admin@technaxis.com',
      password: 'password',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken;

    const promoCodeId = (Math.floor(Math.random() * Math.floor(10))) * 10 + 1;


    const r = await apiProvider().promocode().GetPromoCode(token, promoCodeId);


    expect(r.body.result.id).toBe(promoCodeId);
    expect(r.status).toBe(200);
  });


  test('PUT Promocode', async () => {

    const data = {
      login: 'admin@technaxis.com',
      password: 'password',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken;

    //console.log('token ', token);

    const promoCodeId = (Math.floor(Math.random() * Math.floor(10))) * 10 + 1; //80
    console.log('promoCodeId = ', promoCodeId);

    const demo = new PromoCodeBuilder();
    const params = demo // поля котрые отправляем
      .PromoCode_activationDate()
      .PromoCode_code()
      .PromoCode_discount()
      .PromoCode_discountType()
      .PromoCode_disposable()
      .PromoCode_expirationDate()
      .PromoCode_name()
      .generate();

    const r = await apiProvider().promocode().UpdatePromoCode(token, promoCodeId, params);
    //console.log(r.text);

    expect(r.body.result.id).toBe(promoCodeId);
    expect(r.body.result.code).toBe(params.code);

    expect(r.status).toBe(200);
  });
});

describe('testsuit for clientLevel Controller', () => {
  test('Get user levels', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken;

    //console.log('token ', token);
    const r = await apiProvider().clientLevel().getLevel(token);
    //console.log(r.text);
    expect(r.status).toBe(200);
  });


  test('Get user scales', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken;

    //console.log('token ', token);
    const r = await apiProvider().clientLevel().getUserScales(token);
    //console.log(r.text);
    expect(r.status).toBe(200);
  });

});

describe('testsuit for clientProfile Controller', () => {
  test('Get profile', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken;

    //console.log('token ', token);
    const r = await apiProvider().clientprofile().getProfile(token);

    //console.log(r.text);
    //expect(r.text.name).toEqual("Аленка");

    expect(r.body.result.id).toBe(99);
    expect(r.body.result.email).toEqual('sdgsdg@jlrt.rte');
    expect(r.body.result.phone).toEqual('+79274435697');
    expect(r.status).toBe(200);
  });


  test('Update profile', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken;


    const demo = new ProfileBuilder();
    const params = demo
      .Profile_name()
      .Profile_referralCode()
      .Profile_referralUrlVisible()
      .Profile_newPassword()
      .Profile_oldPassword()
      .generate();

    const r = await apiProvider().clientprofile().updateProfile(token, params);

    console.log(r.text);

    expect(r.body.result.id).toBe(99);
    expect(r.body.result.name).toBe(params.name);
    expect(r.body.result.email).toEqual('sdgsdg@jlrt.rte');
    expect(r.body.result.phone).toEqual('+79274435697');
    expect(r.status).toBe(200);

  });

  test('Get update profile avatar', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken; //получаем токен и сохраняем

    //console.log('token ', token);
    const params = {
      avatarId: 298
    }

    const r = await apiProvider().clientprofile().updateProfileAvatar(token, params);

    //console.log(r.text);

    expect(r.body.result.id).toBe(99);
    expect(r.body.result.email).toEqual('sdgsdg@jlrt.rte');
    expect(r.body.result.phone).toEqual('+79274435697');
    expect(r.body.result.avatar.id).toBe(params.avatarId);
    expect(r.status).toBe(200);
  });


  test('Get profile summary', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken;

    //console.log('token ', token);
    const r = await apiProvider().clientprofile().getProfileSummary(token);

    //console.log(r.text);

    expect(r.body.result.id).toBe(99);
    expect(r.body.result.email).toEqual(data.login); //'sdgsdg@jlrt.rte'
    expect(r.body.result.phone).toEqual('+79274435697');
    expect(r.status).toBe(200);
  });

});

// test('Get Payment info ', async () => {

//   const data = {
//     login: 'sdgsdg@jlrt.rte',
//     password: 'Password56',
//   };

//   const rr = await apiProvider().authorization().post(data);
//   const token = rr.body.result.authToken; //получаем токен и сохраняем

//   console.log('token ', token);

//   const orderId = ;
//   const r = await apiProvider().clientpayment().getPaymentInfo(token, orderId);

//   expect(r.status).toBe(200);
// });


test('Get level user ', async () => {

  const data = {
    login: 'sdgsdg@jlrt.rte',
    password: 'Password56',
  };

  const rr = await apiProvider().authorization().post(data);
  const token = rr.body.result.authToken;

  //console.log('token ', token);
  const r = await apiProvider().clientevent().getEvent(token);

  //console.log(r.text);
  expect(r.status).toBe(200);
});

test('Update ref code ', async () => {

  const data = {
    login: 'sdgsdg@jlrt.rte',
    password: 'Password56',
  };

  const rr = await apiProvider().authorization().post(data);
  const token = rr.body.result.authToken;

  //console.log('token ', token);

  const params = {
    code: faker.finance.bic()
  }

  const r = await apiProvider().clientrefcode().UpdateRefCode(token, params);

  //console.log(r.text);
  //console.log('params.code', params.code);
  expect(r.body.code).toEqual(params.code);
  expect(r.status).toBe(200);
});


describe('testsuit for clientRefferals Controller', () => {
  test('Get refferals statistics ', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken;

    //console.log('token ', token);

    const params = {
      lines: [
        1, 2
      ]
    }

    const r = await apiProvider().clintrefferals().GetRefferals(token, params);

    //console.log(r.text);

    expect(r.status).toBe(200);
  });

  //ВЗЯЛА
  test('Search refferals ', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken;

    //console.log('token ', token);

    const demo = new RefferalsBuilder();
    const params = demo
      .getLimitOfRefferals()
      .getLinesOfRefferals()
      .getOffsetOfRefferals()
      .generate();



    const r = await apiProvider().clintrefferals().SearchRefferals(token, params);

    //console.log(r.text);

    expect(r.body.result.data[0].line).toBe(1);
    expect(r.status).toBe(200);
  });

});


describe('testsuit for client buy ticket Controller', () => {
  //ВЗЯЛА
  test('buy ticket ', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken;

    const demo = new TicketBuilder();
    const params = demo
      .getAmountTicketForBuy()
      .getPartlyOnlinePaymentSumInKopecks()
      .getPaymentMethod(false)
      .getRedirectUrl()
      .generate();

    const r = await apiProvider().clentickets().buyTicket(token, params);

    expect(r.body.result.status).toEqual('PAYMENT_REQUIRED');
    expect(r.status).toBe(201);
  });



  //ВЗЯЛА
  test('buy ticket by wallet', async () => {
    const data = {
      login: 'hft456@mjoi.tyr',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken;

    const demo = new TicketBuilder();
    const params = demo
      .getAmountTicketForBuy()
      .getPaymentMethod(true)
      .generate();

    const r = await apiProvider().clentickets().buyTicket(token, params);

    expect(r.body.result.status).toEqual('SUCCESS');
    expect(r.status).toBe(201);
  });



  test('count tickets ', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken;

    //console.log('token ', token);
    const r = await apiProvider().clentickets().getTicketCount(token);

    //console.log(r.text);
    expect(r.status).toBe(200);
  });


  test('get tickets multiplier ', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken;

    //console.log('token ', token);
    const r = await apiProvider().clentickets().getTicketsMultiplayer(token);

    //console.log(r.text);
    expect(r.status).toBe(200);
  });


  //ВЗЯЛА
  test('search tickets ', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken;

    const demo = new TicketBuilder();
    const params = demo
      .getLimitOfTickets(true)
      .getOffsetOfTickets()
      .getSourcePayment()
      .getWinningTickets()
      .generate();

    const r = await apiProvider().clentickets().SearchTickets(token, params);

    expect(r.body.result.data[0].source).toEqual(params.source);

    expect(r.status).toBe(200);
  });



  test('buy trial tickets by partly online', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken;

    //console.log('token ', token);

    const params = {
      partlyOnlinePaymentSumInKopecks: 1000,
      paymentMethod: 'PARTLY_ONLINE',
      redirectUrl: 'https://matreshka.technaxis.com/cabinet/profile'
    };

    const r = await apiProvider().clentickets().buyTrialTicket(token, params);

    //console.log(r.text);
    expect(r.body.result.status).toEqual('PAYMENT_REQUIRED');
    expect(r.status).toBe(201);
  });


  test('buy trial tickets by online', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken;

    //console.log('token ', token);

    const params = {

      paymentMethod: 'ONLINE',
      redirectUrl: 'https://matreshka.technaxis.com/cabinet/profile'
    };

    const r = await apiProvider().clentickets().buyTrialTicket(token, params);

    //console.log(r.text);
    expect(r.body.result.status).toEqual('PAYMENT_REQUIRED');
    expect(r.status).toBe(201);
  });


  test('get ticket by id', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken;

    //console.log('token ', token);

    const ticketId = 89860;

    const r = await apiProvider().clentickets().getTicketById(token, ticketId);

    //console.log(r.text);

    expect(r.body.result.id).toBe(ticketId);
    expect(r.body.result.number).toEqual('74478138');
    expect(r.body.result.source).toEqual('GIFT');
    expect(r.status).toBe(200);
  });

  //ВЗЯТЬ Истоия выигрышей билета
  test('Search ticket winning history', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken;
    //console.log('token ', token);

    const ticketId = 36534;

    const params = {
      limit: 10,
      offset: 0,
      sorting: [{
        "order": "DESC",
        "type": "WIN_AMOUNT"
      }]
    };

    const r = await apiProvider().clentickets().SearchTicketWinningHistory(token, ticketId, params);

    //console.log(r.text);
    expect(r.status).toBe(200);
  });

});

describe('testsuit for clientWallets Controller', () => {

  test('get my wallet', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken;

    //console.log('token ', token);
    const r = await apiProvider().clientwallet().getMyWallet(token);

    //console.log(r.text);
    expect(r.status).toBe(200);
  });


});


describe('testsuit for clientWithdrawls Controller', () => {
  test('get user limits to withdrawal', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken;
    console.log('token ', token);
    const r = await apiProvider().clientwithdrawls().getUserlimits(token);

    //console.log(r.text);
    expect(r.status).toBe(200);
  });

  test('get all withdrawal providers', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken; //получаем токен и сохраняем

    //console.log('token ', token);
    const r = await apiProvider().clientwithdrawls().getAllWithdraws(token);

    //console.log(r.text);
    expect(r.status).toBe(200);
  });

});

describe('testsuit for Promo Controller', () => {

  test('Page promo', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken; //получаем токен и сохраняем

    //console.log('token ', token);
    const params = {
      limit: 10,
      offset: 0,

    };

    const types = ['MATERIAL', 'ADVERTISING', 'TRAINING'];
    const index = parseInt(Math.random(1, 2));
    //console.log('i ', index);
    const r = await apiProvider().clientpromomaterial().pagePromo(token, params, types[index]);

    //console.log(r.text);
    expect(r.status).toBe(200);
  });

});

describe('testsuit for winningHistories Controller', () => {

  test('page reviews', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken; //получаем токен и сохраняем

    //console.log('token ', token);
    const params = {
      limit: 10,
      offset: 0
    }
    const r = await apiProvider().clientwinninghistories().pageReviews(token, params);

    //console.log(r.text);
    expect(r.status).toBe(200);
  });


  test('Search winning histories', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken; //получаем токен и сохраняем

    //console.log('token ', token);
    const params = {
      limit: 10,
      offset: 0,
      referralLines: [
        1, 2
      ],
      referrerLines: [
        1
      ],
      withReview: true
    }

    const r = await apiProvider().clientwinninghistories().searchWinningHistories(token, params);

    //console.log(r.text);
    expect(r.status).toBe(200);
  });


  test('Get winning history', async () => {

    const data = {
      login: 'sdgsdg@jlrt.rte',
      password: 'Password56',
    };

    const rr = await apiProvider().authorization().post(data);
    const token = rr.body.result.authToken; //получаем токен и сохраняем

    const historyId = 58225;
    const r = await apiProvider().clientwinninghistories().getWinningHistory(token, historyId);

    expect(r.body.result.id).toBe(historyId);
    //expect(r.body.result.winner.id).toBe(37);
    expect(r.status).toBe(200);
  });

});