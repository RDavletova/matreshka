import faker from 'faker';

const CountryBuilder = function CountryBuilder(){ //функция- это объект

  this.SearchCities_important = function SearchCities_important(){ 
      this.important = true;//
      return this; 
  };

  this.SearchCities_limit = function SearchCities_limit(){ 
    this.limit = 10;
    return this; 
  };


  this.SearchCities_offset = function SearchCities_offset(){ 
    this.offset = 0;
    return this; 
  };

  this.SearchCities_useDefaultCountry = function SearchCities_useDefaultCountry(){ 
    this.useDefaultCountry = true;
    return this; 
  };

  this.SearchCities_query = function SearchCities_query(){ 
    this.query = "Арташат";
    return this; 
  };
  
  
  this.generate = function generate(){        
      const fields = Object.getOwnPropertyNames(this);
      console.log('fields:', fields); 
      
      const data = {};
      //mas.forEach((i) 
      fields.forEach((fieldName) => {
          
          if(this[fieldName] && typeof this[fieldName] !== 'function'){
            data[fieldName] = this[fieldName];
          }   
      });
      console.log(data); 

      return data;   
  }
};


export { CountryBuilder };
