"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TicketBuilder = void 0;

var _faker = _interopRequireDefault(require("faker"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var TicketBuilder = function TicketBuilder() {
  function randomInteger(min, max) {
    var rand = min - 0.5 + Math.random() * (max - min + 1);
    return Math.round(rand);
  }

  this.getAmountTicketForBuy = function getAmountTicketForBuy() {
    this.amount = randomInteger(1, 3);
    return this;
  };

  this.getPartlyOnlinePaymentSumInKopecks = function getPartlyOnlinePaymentSumInKopecks() {
    this.partlyOnlinePaymentSumInKopecks = randomInteger(500, 1000);
    return this;
  };

  this.getPaymentMethod = function getPaymentMethod(flag) {
    if (flag) {
      this.paymentMethod = 'MATRESHKA_WALLET';
    } else {
      this.paymentMethod = 'PARTLY_ONLINE';
    }

    return this;
  };

  this.getRedirectUrl = function getRedirectUrl() {
    this.redirectUrl = 'https://matreshka.technaxis.com/cabinet';
    return this;
  };

  this.getLimitOfTickets = function getLimitOfTickets(flag) {
    if (flag) {
      this.limit = randomInteger(1, 100);
    } else {
      this.limit = 0;
    }

    return this;
  };

  this.getOffsetOfTickets = function getOffsetOfTickets() {
    this.offset = 0;
    return this;
  };

  this.getSourcePayment = function getSourcePayment() {
    var listTypePayment = ['PAYMENT', 'GIFT'];
    this.source = listTypePayment[randomInteger(0, 1)];
    return this;
  };

  this.getWinningTickets = function getWinningTickets() {
    this.winning = _faker["default"].random["boolean"]();
    return this;
  };

  this.generate = function generate() {
    var _this = this;

    var fields = Object.getOwnPropertyNames(this);
    console.log('fields:', fields);
    var data = {};
    fields.forEach(function (fieldName) {
      if (_this[fieldName] && typeof _this[fieldName] !== 'function') {
        data[fieldName] = _this[fieldName];
      }
    });
    console.log('data from generate method ', data);
    return data;
  };
};

exports.TicketBuilder = TicketBuilder;