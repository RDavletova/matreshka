"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CountryBuilder = void 0;

var _faker = _interopRequireDefault(require("faker"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var CountryBuilder = function CountryBuilder() {
  //функция- это объект
  this.SearchCities_important = function SearchCities_important() {
    this.important = true; //

    return this;
  };

  this.SearchCities_limit = function SearchCities_limit() {
    this.limit = 10;
    return this;
  };

  this.SearchCities_offset = function SearchCities_offset() {
    this.offset = 0;
    return this;
  };

  this.SearchCities_useDefaultCountry = function SearchCities_useDefaultCountry() {
    this.useDefaultCountry = true;
    return this;
  };

  this.SearchCities_query = function SearchCities_query() {
    this.query = "Арташат";
    return this;
  };

  this.generate = function generate() {
    var _this = this;

    var fields = Object.getOwnPropertyNames(this);
    console.log('fields:', fields);
    var data = {}; //mas.forEach((i) 

    fields.forEach(function (fieldName) {
      if (_this[fieldName] && typeof _this[fieldName] !== 'function') {
        data[fieldName] = _this[fieldName];
      }
    });
    console.log(data);
    return data;
  };
};

exports.CountryBuilder = CountryBuilder;