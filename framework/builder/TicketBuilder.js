import faker from 'faker';

const TicketBuilder = function TicketBuilder() {

  function randomInteger(min, max) {
    let rand = min - 0.5 + Math.random() * (max - min + 1);
    return Math.round(rand);
  }

  this.getAmountTicketForBuy = function getAmountTicketForBuy() {
    this.amount = randomInteger(1, 3);
    return this;
  };


  this.getPartlyOnlinePaymentSumInKopecks = function getPartlyOnlinePaymentSumInKopecks() {
    this.partlyOnlinePaymentSumInKopecks = randomInteger(500, 1000);
    return this;
  };

  this.getPaymentMethod = function getPaymentMethod(flag) {
    if (flag) {
      this.paymentMethod = 'MATRESHKA_WALLET';
    } else {
      this.paymentMethod = 'PARTLY_ONLINE';
    }
    return this;
  };



  this.getRedirectUrl = function getRedirectUrl() {
    this.redirectUrl = 'https://matreshka.technaxis.com/cabinet';
    return this;
  };

  this.getLimitOfTickets = function getLimitOfTickets(flag) {
    if (flag) {
      this.limit = randomInteger(1, 100);
    } else {
      this.limit = 0;
    }
    return this;
  };

  this.getOffsetOfTickets = function getOffsetOfTickets() {
    this.offset = 0;
    return this;
  };

  this.getSourcePayment = function getSourcePayment() {
    const listTypePayment = ['PAYMENT', 'GIFT'];
    this.source = listTypePayment[randomInteger(0, 1)];
    return this;
  };

  this.getWinningTickets = function getWinningTickets() {
    this.winning = faker.random.boolean();
    return this;
  };


  this.generate = function generate() {
    const fields = Object.getOwnPropertyNames(this);
    console.log('fields:', fields);
    const data = {};

    fields.forEach((fieldName) => {

      if (this[fieldName] && typeof this[fieldName] !== 'function') {
        data[fieldName] = this[fieldName];
      }
    });
    console.log('data from generate method ', data);

    return data;
  }

};

export {
  TicketBuilder
};
