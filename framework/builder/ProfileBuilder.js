import faker from 'faker';

const ProfileBuilder = function ProfileBuilder() {

  this.Profile_name = function Profile_name() {
    this.name = faker.name.firstName();
    return this; // Profile_name.name
  };

  this.Profile_referralCode = function Profile_referralCode() {
    this.referralCode = faker.finance.bic();
    return this;
  };


  this.Profile_referralUrlVisible = function Profile_referralUrlVisible() {
    this.referralUrlVisible = false;
    return this;
  };

  this.Profile_newPassword = function Profile_newPassword() {
    this.newPassword = 'Password56';
    return this;
  };

  this.Profile_oldPassword = function Profile_oldPassword() {
    this.oldPassword = 'Password56';
    return this;
  };




  this.generate = function generate() {
    const fields = Object.getOwnPropertyNames(this);
    console.log('fields:', fields);

    const data = {};

    fields.forEach((fieldName) => {

      if (this[fieldName] && typeof this[fieldName] !== 'function') {
        data[fieldName] = this[fieldName];
      }
    });
    console.log('data from generate method ', data);

    return data;
  }

};

export {
  ProfileBuilder
};