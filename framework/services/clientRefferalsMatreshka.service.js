import supertest from 'supertest';
import {
  urls
} from '../config';
import {
  decorateService
} from '../../lib/decorate';

const ClientRefferals = function ClientRefferals() {

  this.GetRefferals = async function (token, params) {
    reporter.startStep("Вызываем POST /v1/client/referrals");

    const r = await supertest(urls.matreshka)
      .post('/v1/client/referrals') //метод POST
      .set('Authorization', `Bearer ${token}`)
      .send(params);

    //console.log(r);
    reporter.endStep();
    return r;
  };

  this.SearchRefferals = async function (token, params) {
    reporter.startStep("Вызываем POST /v1/client/referrals/search");

    const r = await supertest(urls.matreshka)
      .post('/v1/client/referrals/search') //метод POST
      .set('Authorization', `Bearer ${token}`)
      .send(params);

    //console.log(r);
    reporter.endStep();
    return r;
  };


};

decorateService(ClientRefferals);



export {
  ClientRefferals
};