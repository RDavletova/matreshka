import supertest from 'supertest';
import {
  urls
} from '../config';
import {
  decorateService
} from '../../lib/decorate';

const ClientWinningHistories = function ClientWinningHistories() {

  this.pageReviews = async function (token, params) {
    reporter.startStep("Вызываем POST /v1/winning-histories/reviews/page");

    const r = await supertest(urls.matreshka)
      .post('/v1/winning-histories/reviews/page') //метод POST
      .set('Authorization', `Bearer ${token}`)
      .send(params);


    reporter.endStep();
    return r;
  };

  this.searchWinningHistories = async function (token, params) {
    reporter.startStep("Вызываем POST /v1/winning-histories/search");

    const r = await supertest(urls.matreshka)
      .post('/v1/winning-histories/search') //метод POST
      .set('Authorization', `Bearer ${token}`)
      .send(params);


    reporter.endStep();
    return r;
  };

  this.getWinningHistory = async function (token, historyId) {
    reporter.startStep("Вызываем GET /v1/winning-histories/' + `${historyId}`");

    const r = await supertest(urls.matreshka)
      .get('/v1/winning-histories/' + `${historyId}`) //метод GET
      .set('Authorization', `Bearer ${token}`)
      .send();


    reporter.endStep();
    return r;
  };


};

decorateService(ClientWinningHistories);
export {
  ClientWinningHistories
};