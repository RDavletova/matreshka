import supertest from 'supertest';
import { urls } from '../config';
import { decorateService } from '../../lib/decorate'; 

const ClientWithdrawls = function ClientWithdrawls() { 

  this.getUserlimits = async function (token) { 
    reporter.startStep("Вызываем  GET /v1/client/withdrawals/limits");

    const r = await supertest(urls.matreshka)
    .get('/v1/client/withdrawals/limits') //метод GET
    .set('Authorization', `Bearer ${token}`)
    .send();
    
    //console.log(r);
    reporter.endStep();
    return r;
  };

  this.getAllWithdraws = async function (token) { 
    reporter.startStep("Вызываем GET /v1/client/withdrawals/providers");

    const r = await supertest(urls.matreshka)
    .get('/v1/client/withdrawals/providers') //метод GET
    .set('Authorization', `Bearer ${token}`)
    .send();
    
    //console.log(r);
    reporter.endStep();
    return r;
  };

  
};

decorateService(ClientWithdrawls); 
export { ClientWithdrawls }; 
