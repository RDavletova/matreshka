"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ClientRefCode = void 0;

var _supertest = _interopRequireDefault(require("supertest"));

var _config = require("../config");

var _decorate = require("../../lib/decorate");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var ClientRefCode = function ClientRefCode() {
  this.UpdateRefCode = function _callee(token, params) {
    var r;
    return regeneratorRuntime.async(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            reporter.startStep("Вызываем PUT /v1/client/refcode");
            _context.next = 3;
            return regeneratorRuntime.awrap((0, _supertest["default"])(_config.urls.matreshka).put('/v1/client/refcode') //метод PUT
            .set('Authorization', "Bearer ".concat(token)).send(params));

          case 3:
            r = _context.sent;
            //console.log(r);
            reporter.endStep();
            return _context.abrupt("return", r);

          case 6:
          case "end":
            return _context.stop();
        }
      }
    });
  };
};

exports.ClientRefCode = ClientRefCode;
(0, _decorate.decorateService)(ClientRefCode);