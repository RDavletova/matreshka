"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Authorization = void 0;

var _supertest = _interopRequireDefault(require("supertest"));

var _config = require("../config");

var _decorate = require("../../lib/decorate");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Authorization = function Authorization() {
  this.post = function _callee(params) {
    var r;
    return regeneratorRuntime.async(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            reporter.startStep("Вызываем post /v1/client/login");
            _context.next = 3;
            return regeneratorRuntime.awrap((0, _supertest["default"])(_config.urls.matreshka).post('/v1/client/login') //метод POST 
            .send(params));

          case 3:
            r = _context.sent;
            reporter.endStep(); //console.log(r);

            return _context.abrupt("return", r);

          case 6:
          case "end":
            return _context.stop();
        }
      }
    });
  };
};

exports.Authorization = Authorization;
(0, _decorate.decorateService)(Authorization);