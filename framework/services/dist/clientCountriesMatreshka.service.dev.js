"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Countries = void 0;

var _supertest = _interopRequireDefault(require("supertest"));

var _config = require("../config");

var _decorate = require("../../lib/decorate");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Countries = function Countries() {
  this.getAllCountries = function _callee(token) {
    var r;
    return regeneratorRuntime.async(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            reporter.startStep("Вызываем GET /v1/client/countries");
            _context.next = 3;
            return regeneratorRuntime.awrap((0, _supertest["default"])(_config.urls.matreshka).get('/v1/client/countries') //метод GET
            .set('Authorization', "Bearer ".concat(token)).send());

          case 3:
            r = _context.sent;
            //console.log(r);
            reporter.endStep();
            return _context.abrupt("return", r);

          case 6:
          case "end":
            return _context.stop();
        }
      }
    });
  };

  this.SearchCities = function _callee2(token, params) {
    var r;
    return regeneratorRuntime.async(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            reporter.startStep("Вызываем POST /v1/client/countries/cities/search");
            _context2.next = 3;
            return regeneratorRuntime.awrap((0, _supertest["default"])(_config.urls.matreshka).post('/v1/client/countries/cities/search') //метод POST
            .set('Authorization', "Bearer ".concat(token)).send(params));

          case 3:
            r = _context2.sent;
            reporter.endStep();
            return _context2.abrupt("return", r);

          case 6:
          case "end":
            return _context2.stop();
        }
      }
    });
  };

  this.postCities = function _callee3(token, params) {
    var countryId, r;
    return regeneratorRuntime.async(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            reporter.startStep("Вызываем POST /v1/client/countries/ ' + countryId + '/cities/search");
            countryId = 6; //countryId - path параметр

            _context3.next = 4;
            return regeneratorRuntime.awrap((0, _supertest["default"])(_config.urls.matreshka).post('/v1/client/countries/ ' + countryId + '/cities/search') //метод POST
            .set('Authorization', "Bearer ".concat(token)).send(params));

          case 4:
            r = _context3.sent;
            reporter.endStep();
            return _context3.abrupt("return", r);

          case 7:
          case "end":
            return _context3.stop();
        }
      }
    });
  };
};

exports.Countries = Countries;
(0, _decorate.decorateService)(Countries);