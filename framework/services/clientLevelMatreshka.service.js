import supertest from 'supertest';
import {
  urls
} from '../config';
import {
  decorateService
} from '../../lib/decorate';

const ClientLevel = function ClientLevel() {

  this.getLevel = async function (token) {
    reporter.startStep("Вызываем GET /v1/client/levels");

    const r = await supertest(urls.matreshka)
      .get('/v1/client/levels') //метод GET
      .set('Authorization', `Bearer ${token}`)
      .send();

    //console.log(r);
    reporter.endStep();
    return r;
  };


  this.getUserScales = async function (token) {
    reporter.startStep("Вызываем GET /v1/client/levels/scales");

    const r = await supertest(urls.matreshka)
      .get('/v1/client/levels/scales') //метод GET
      .set('Authorization', `Bearer ${token}`)
      .send();

    //console.log(r);
    reporter.endStep();
    return r;
  };



};

decorateService(ClientLevel);

export {
  ClientLevel
};