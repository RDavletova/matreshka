import supertest from 'supertest';
import {
  urls
} from '../config';
import {
  decorateService
} from '../../lib/decorate';

const ClientRefCode = function ClientRefCode() {

  this.UpdateRefCode = async function (token, params) {
    reporter.startStep("Вызываем PUT /v1/client/refcode");

    const r = await supertest(urls.matreshka)
      .put('/v1/client/refcode') //метод PUT
      .set('Authorization', `Bearer ${token}`)
      .send(params);

    //console.log(r);
    reporter.endStep();
    return r;
  };




};

decorateService(ClientRefCode);



export {
  ClientRefCode
};