import supertest from 'supertest';
import { urls } from '../config';
import { decorateService } from '../../lib/decorate'; 

const ClientWallet = function ClientWallet() { 

  this.getMyWallet = async function (token) { 
    reporter.startStep("Вызываем GET /v1/client/wallets");
    
    const r = await supertest(urls.matreshka)
    .get('/v1/client/wallets')//метод GET
    .set('Authorization', `Bearer ${token}`)
    .send();
    
    //console.log(r);
    reporter.endStep();
    return r;
  };


  

  
};

decorateService(ClientWallet); 
export { ClientWallet }; 
