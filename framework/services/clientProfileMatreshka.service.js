import supertest from 'supertest';
import {
  urls
} from '../config';
import {
  decorateService
} from '../../lib/decorate';

const ClientProfile = function ClientProfile() {

  this.getProfile = async function (token) {
    reporter.startStep("Вызываем GET /v1/client/profile");

    const r = await supertest(urls.matreshka)
      .get('/v1/client/profile') //метод GET
      .set('Authorization', `Bearer ${token}`)
      .send();

    //console.log(r);
    reporter.endStep();
    return r;
  };

  this.updateProfile = async function (token, params) {
    reporter.startStep("Вызываем PATCH /v1/client/profile");

    const r = await supertest(urls.matreshka)
      .patch('/v1/client/profile') //метод PATCH
      .set('Authorization', `Bearer ${token}`)
      .send(params);

    //console.log(r);
    reporter.endStep();
    return r;
  };

  this.updateProfileAvatar = async function (token, avatarId) {
    reporter.startStep("Вызываем  PUT /v1/client/profile/avatar");

    const r = await supertest(urls.matreshka)
      .put('/v1/client/profile/avatar') //метод PUT
      .set('Authorization', `Bearer ${token}`)
      .send(avatarId);

    //console.log(r);
    reporter.endStep();
    return r;
  };

  this.getProfileSummary = async function (token) {
    reporter.startStep("Вызываем GET /v1/client/profile/summary");

    const r = await supertest(urls.matreshka)
      .get('/v1/client/profile/summary') //метод GET
      .set('Authorization', `Bearer ${token}`)
      .send();

    //console.log(r);
    reporter.endStep();
    return r;
  };


};

decorateService(ClientProfile);

//export default new ClientProfile(); 

export {
  ClientProfile
};