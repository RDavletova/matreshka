import supertest from 'supertest';
import { urls } from '../config';
import { decorateService } from '../../lib/decorate'; 

const ClientPromoMaterials = function ClientPromoMaterials() { 

  this.pagePromo = async function (token, params, type) { 
    reporter.startStep("Вызываем  POST /v1/promo/page");

    const r = await supertest(urls.matreshka)
    .post('/v1/promo/page') //метод POST
    .query({ type: `${type}` }) // передаем query парметр type
    .set('Authorization', `Bearer ${token}`)
    .send(params);
    
    //console.log(r);
    reporter.endStep();
    return r;
  };

  
  
};

decorateService(ClientPromoMaterials); 
export { ClientPromoMaterials }; 
