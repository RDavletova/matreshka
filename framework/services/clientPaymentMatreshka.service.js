import supertest from 'supertest';
import {
  urls
} from '../config';
import {
  decorateService
} from '../../lib/decorate';

const ClientPayment = function ClientPayment() {

  this.getPaymentInfo = async function (token, orderId) {
    reporter.startStep("Вызываем  GET /v1/client/payments/' + `${orderId}`");

    const r = await supertest(urls.matreshka)
      .get('/v1/client/payments/' + `${orderId}`) //метод GET
      .set('Authorization', `Bearer ${token}`)
      .send();

    //console.log(r);
    reporter.endStep();
    return r;
  };





};

decorateService(ClientPayment);
export {
  ClientPayment
};