import supertest from 'supertest';
import { urls } from '../config';
import { decorateService } from '../../lib/decorate'; 

const ClientEvent = function ClientEvent() { 

  this.getEvent = async function (token) { 
    reporter.startStep("Вызываем GET /v1/client/events");
    
    const r = await supertest(urls.matreshka)
    .get('/v1/client/events')//метод GET
    .set('Authorization', `Bearer ${token}`)
    .send();
    
    //console.log(r);
    reporter.endStep(); 
    return r;
  };


  

  
};

decorateService(ClientEvent); 



export { ClientEvent }; 
