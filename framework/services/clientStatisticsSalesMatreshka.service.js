import supertest from 'supertest';
import {
  urls
} from '../config';
import {
  decorateService
} from '../../lib/decorate';

const ClientStatisticsSales = function ClientStatisticsSales() {

  this.getStatisticsSaleForPeriod = async function (token, params) {
    reporter.startStep("Вызываем POST /v1/client/statistics/sales/period");

    const r = await supertest(urls.matreshka)
      .post('/v1/client/statistics/sales/period') //метод POST
      .set('Authorization', `Bearer ${token}`)
      .send(params);

    //console.log(r);
    reporter.endStep();
    return r;
  };




};

decorateService(ClientStatisticsSales);



export {
  ClientStatisticsSales
};