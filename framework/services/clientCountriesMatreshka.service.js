import supertest from 'supertest';
import {
  urls
} from '../config';
import {
  decorateService
} from '../../lib/decorate';

const Countries = function Countries() {

  this.getAllCountries = async function (token) {
    reporter.startStep("Вызываем GET /v1/client/countries");

    const r = await supertest(urls.matreshka)
      .get('/v1/client/countries') //метод GET
      .set('Authorization', `Bearer ${token}`)
      .send()

    //console.log(r);
    reporter.endStep();
    return r;
  };


  this.SearchCities = async function (token, params) {
    reporter.startStep("Вызываем POST /v1/client/countries/cities/search");

    const r = await supertest(urls.matreshka)
      .post('/v1/client/countries/cities/search') //метод POST
      .set('Authorization', `Bearer ${token}`)
      .send(params);

    reporter.endStep();
    return r;
  };


  this.postCities = async function (token, params) {
    reporter.startStep("Вызываем POST /v1/client/countries/ ' + countryId + '/cities/search");

    const countryId = 6; //countryId - path параметр
    const r = await supertest(urls.matreshka)
      .post('/v1/client/countries/ ' + countryId + '/cities/search') //метод POST
      .set('Authorization', `Bearer ${token}`)
      .send(params);

    reporter.endStep();
    return r;
  };


};

decorateService(Countries);



export {
  Countries
};