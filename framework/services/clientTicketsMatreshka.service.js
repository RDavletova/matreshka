import supertest from 'supertest';
import {
  urls
} from '../config';
import {
  decorateService
} from '../../lib/decorate';

const ClientTickets = function ClientTickets() {

  this.buyTicket = async function (token, params) {
    reporter.startStep("Вызываем POST /v1/client/tickets/buy");

    const r = await supertest(urls.matreshka)
      .post('/v1/client/tickets/buy') //метод POST
      .set('Authorization', `Bearer ${token}`)
      .send(params);

    //console.log(r);
    reporter.endStep();
    return r;
  };


  this.getTicketCount = async function (token) {
    reporter.startStep("Вызываем GET /v1/client/tickets/count");

    const r = await supertest(urls.matreshka)
      .get('/v1/client/tickets/count') //метод GET
      .set('Authorization', `Bearer ${token}`)
      .send();


    reporter.endStep();
    return r;
  };

  this.getTicketsMultiplayer = async function (token) {
    reporter.startStep("Вызываем /v1/client/tickets/multiplier");

    const r = await supertest(urls.matreshka)
      .get('/v1/client/tickets/multiplier') //метод GET
      .set('Authorization', `Bearer ${token}`)
      .send();


    reporter.endStep();
    return r;
  };

  this.SearchTickets = async function (token, params) {
    reporter.startStep("Вызываем POST /v1/client/tickets/search");

    const r = await supertest(urls.matreshka)
      .post('/v1/client/tickets/search') //метод POST
      .set('Authorization', `Bearer ${token}`)
      .send(params);


    reporter.endStep();
    return r;
  };

  this.buyTrialTicket = async function (token, params) {
    reporter.startStep("Вызываем POST /v1/client/tickets/trial/buy");

    const r = await supertest(urls.matreshka)
      .post('/v1/client/tickets/trial/buy') //метод POST
      .set('Authorization', `Bearer ${token}`)
      .send(params);


    reporter.endStep();
    return r;
  };


  this.getTicketById = async function (token, ticketId) {
    reporter.startStep("Вызываем GET /v1/client/tickets/' + `${ticketId}`");

    const r = await supertest(urls.matreshka)
      .get('/v1/client/tickets/' + `${ticketId}`) //метод GET
      .set('Authorization', `Bearer ${token}`)
      .send();


    reporter.endStep();
    return r;
  };

  this.SearchTicketWinningHistory = async function (token, ticketId, params) {
    reporter.startStep("Вызываем POST /v1/client/tickets/' + `${ticketId}` + '/winning-history/page");

    const r = await supertest(urls.matreshka)
      .post('/v1/client/tickets/' + `${ticketId}` + '/winning-history/page') //метод POST
      .set('Authorization', `Bearer ${token}`)
      .send(params);


    reporter.endStep();
    return r;
  };


};

decorateService(ClientTickets);



export {
  ClientTickets
};