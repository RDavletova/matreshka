import supertest from 'supertest';
import { urls } from '../config';

const AdminPromocode = function AdminPromocode() { //

    this.CreatePromoCode = async function (token, params) { 
        reporter.startStep("Вызываем post /v1/admin/promo-codes");

        let r = await supertest(urls.matreshka)
            .post('/v1/admin/promo-codes') //метод post
            .set('Authorization', `Bearer ${token}`)
            .send(params);

        reporter.endStep();
        return r;
    };


    this.GetPromoCode = async function (token, promoCodeId) { 
        reporter.startStep("Вызываем get /v1/admin/promo-codes/{promoCodeId}");

        let r = await supertest(urls.matreshka)
            .get('/v1/admin/promo-codes/' + `${promoCodeId}`) //метод get  /v1/admin/promo-codes/{promoCodeId}
            .set('Authorization', `Bearer ${token}`)
            .send();
        reporter.endStep();
        
        return r;
    };

    this.UpdatePromoCode = async function (token, promoCodeId, params){
        reporter.startStep("Вызываем put /v1/admin/promo-codes/{promoCodeId}");

        let r = await supertest(urls.matreshka)
            .put('/v1/admin/promo-codes/' + `${promoCodeId}`) //метод put  /v1/admin/promo-codes/{promoCodeId}
            .set('Authorization', `Bearer ${token}`)
            .send(params);
        reporter.endStep();
        
        return r;
    };

    

    

}

export { AdminPromocode }; 


