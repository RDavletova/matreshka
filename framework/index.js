// здесь фукнция посредник
import { Authorization } from './services/index';
import { Countries } from './services/index';
import { AdminPromocode } from './services/index';
import { ClientLevel } from './services/index';
import { ClientProfile } from './services/index';
import { ClientEvent } from './services/index';
import { ClientPayment } from './services/index';
import { ClientRefCode } from './services/index';
import { ClientRefferals } from './services/index';
import { ClientTickets } from './services/index';
import { ClientWallet } from './services/index';
import { ClientWithdrawls } from './services/index';
import { ClientPromoMaterials } from './services/index';
import { ClientWinningHistories } from './services/index';
import { ClientStatisticsSales } from './services/index';



const apiProvider = () => ({
  authorization: () => new Authorization(),
  countries: () => new Countries(),
  promocode: () => new AdminPromocode(),
  clientLevel: () => new ClientLevel(),
  clientprofile: () => new ClientProfile(),
  clientevent: () => new ClientEvent(),
  clientpayment: () => new ClientPayment(),
  clientrefcode: () => new ClientRefCode(),
  clintrefferals: () => new ClientRefferals(),
  clentickets: () => new ClientTickets(),
  clientwallet: () => new ClientWallet(),
  clientwithdrawls: () => new ClientWithdrawls(),
  clientpromomaterial: () => new ClientPromoMaterials(),
  clientwinninghistories: () => new ClientWinningHistories(),
  clientstatisticssales: () => new ClientStatisticsSales()
});

export { apiProvider };