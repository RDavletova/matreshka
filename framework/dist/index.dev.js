"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.apiProvider = void 0;

var _index = require("./services/index");

// здесь фукнция посредник
var apiProvider = function apiProvider() {
  return {
    authorization: function authorization() {
      return new _index.Authorization();
    },
    countries: function countries() {
      return new _index.Countries();
    },
    promocode: function promocode() {
      return new _index.AdminPromocode();
    },
    clientLevel: function clientLevel() {
      return new _index.ClientLevel();
    },
    clientprofile: function clientprofile() {
      return new _index.ClientProfile();
    },
    clientevent: function clientevent() {
      return new _index.ClientEvent();
    },
    clientpayment: function clientpayment() {
      return new _index.ClientPayment();
    },
    clientrefcode: function clientrefcode() {
      return new _index.ClientRefCode();
    },
    clintrefferals: function clintrefferals() {
      return new _index.ClientRefferals();
    },
    clentickets: function clentickets() {
      return new _index.ClientTickets();
    },
    clientwallet: function clientwallet() {
      return new _index.ClientWallet();
    },
    clientwithdrawls: function clientwithdrawls() {
      return new _index.ClientWithdrawls();
    },
    clientpromomaterial: function clientpromomaterial() {
      return new _index.ClientPromoMaterials();
    },
    clientwinninghistories: function clientwinninghistories() {
      return new _index.ClientWinningHistories();
    },
    clientstatisticssales: function clientstatisticssales() {
      return new _index.ClientStatisticsSales();
    }
  };
};

exports.apiProvider = apiProvider;